import React from 'react';

import Dashboard from './components/Dashboard';
import Map from './components/Map';

function App() {
  return (
    <React.Fragment>
      <Dashboard />
      <Map />
    </React.Fragment>
  );
}

export default App;
