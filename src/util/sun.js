import * as THREE from 'three';
import sunCalc from 'suncalc';

let ts = Date.now();
let tickTime = 0;
let raf = null;
let currentSpeed = null;

export const position = () => {
  const lang = 127.1171909;
  const lati = 37.3550181;

  const { azimuth, altitude } = sunCalc.getPosition(new Date(ts), lati, lang);

  const vector = new THREE.Vector3(0, 0, 1);
  const xAxis = new THREE.Vector3(1, 0, 0);
  const yAxis = new THREE.Vector3(0, 1, 0);

  vector
    .applyAxisAngle(xAxis, -altitude)
    .applyAxisAngle(yAxis, -azimuth)
    .multiplyScalar(700);
  return [vector.x, vector.y, vector.z];
};

export const timestamp = t => {
  if (t === undefined) return ts;
  ts = t;
};

export const play = () => {
  pause();

  const tick = () => {
    ts += (Date.now() - tickTime) * 1000 * currentSpeed;
    tickTime = Date.now();
    raf = requestAnimationFrame(tick);
  };

  tickTime = Date.now();
  raf = requestAnimationFrame(tick);
};

export const pause = () => {
  if (raf) {
    cancelAnimationFrame(raf);
    raf = null;
  }
};

export const speed = speed => {
  currentSpeed = speed;
};
