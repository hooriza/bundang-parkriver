import React, { useRef } from 'react';
import { useFrame } from 'react-three-fiber';
import * as SUN from '../util/sun';

const width = window.innerWidth;
const height = window.innerHeight;

function Sunlight() {
  const lightRef = useRef();
  const sunRef = useRef();

  useFrame(() => {
    const sunPosition = SUN.position();
    lightRef.current.position.set(...sunPosition);
    sunRef.current.position.set(...sunPosition);
  });

  return (
    <React.Fragment>
      <directionalLight
        ref={lightRef}
        color={0xffffff}
        intensity={1.2}
        castShadow
        shadowCameraNear={-1000}
        shadowCameraFar={3000}
        shadowMapWidth={width * 2}
        shadowMapHeight={height * 2}
        shadowCameraLeft={-width / 2}
        shadowCameraRight={width / 2}
        shadowCameraTop={height / 2}
        shadowCameraBottom={-height / 2}
        position={SUN.position()}
      />
      <directionalLight
        color={0xffffff}
        intensity={0.1}
        shadowCameraNear={-1000}
        shadowCameraFar={5000}
        shadowMapWidth={width * 2}
        shadowMapHeight={height * 2}
        shadowCameraLeft={-width / 2}
        shadowCameraRight={width / 2}
        shadowCameraTop={height / 2}
        shadowCameraBottom={-height / 2}
        position={[-900, 600, 900]}
      />
      <directionalLight
        color={0xffffff}
        intensity={0.1}
        shadowCameraNear={-1000}
        shadowCameraFar={5000}
        shadowMapWidth={width * 2}
        shadowMapHeight={height * 2}
        shadowCameraLeft={-width / 2}
        shadowCameraRight={width / 2}
        shadowCameraTop={height / 2}
        shadowCameraBottom={-height / 2}
        position={[-900, 600, -900]}
      />
      <mesh ref={sunRef}>
        <sphereGeometry attach="geometry" args={[20, 20, 20]} />
        <meshBasicMaterial attach="material" color={0xffff00} />
      </mesh>
    </React.Fragment>
  );
}

export default Sunlight;
