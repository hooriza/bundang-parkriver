import React, { useState, useCallback, useRef, useEffect } from 'react';
import DatePicker, { registerLocale } from 'react-datepicker';
import Slider from 'rc-slider';
import classNames from 'classnames';
import { format } from 'date-fns';
import ko from 'date-fns/locale/ko';
import * as SUN from '../util/sun';

import 'rc-slider/assets/index.css';
import 'react-datepicker/dist/react-datepicker.css';

registerLocale('ko', ko);

const MINUTE = 1000 * 60;
const INTERVAL = 10 * MINUTE;

function Dashboard() {
  const [startDate, setStartDate] = useState(Date.now());
  const [speed, setSpeed] = useState(5);
  const [playing, setPlaying] = useState(true);

  const lastPlaying = useRef();

  if (playing) SUN.play();
  else SUN.pause();
  SUN.speed(speed);

  const handleClickButton = useCallback(event => {
    event.preventDefault();
    setPlaying(playing => !playing);
  }, []);

  const handleChangePicker = useCallback(
    date => {
      setStartDate(date);
      SUN.timestamp(date.getTime());
    },
    [setStartDate],
  );

  const handleOpenPicker = useCallback(() => {
    lastPlaying.current = playing;
    setPlaying(false);
  }, [playing]);

  const handleClosePicker = useCallback(() => {
    setPlaying(lastPlaying.current);
  }, [lastPlaying]);

  const handleChangeSpeed = useCallback(props => {
    const { value, dragging, index, ...restProps } = props;
    setSpeed(value);
    return <Slider.Handle value={value} {...restProps} />;
  }, []);

  useEffect(() => {
    const timer = setInterval(() => setStartDate(SUN.timestamp()), 100);
    return () => clearInterval(timer);
  });

  const trimedStartDate = Math.round(startDate / INTERVAL) * INTERVAL;

  return (
    <div className="dashboard">
      <DatePicker
        locale="ko"
        dateFormatCalendar="yyyy년 LLLL"
        selected={trimedStartDate}
        onChange={handleChangePicker}
        showTimeSelect
        timeFormat="HH:mm"
        timeIntervals={INTERVAL / MINUTE}
        timeCaption="시간"
        dateFormat="yyyy년 MMMM d일 aa h시 mm분"
        className="datetime-picker"
        customInput={
          <button>
            {format(trimedStartDate, 'yyyy년 MMMM d일 aa h시 mm분', {
              locale: ko,
            })}
          </button>
        }
        onCalendarOpen={handleOpenPicker}
        onCalendarClose={handleClosePicker}
      />
      <div className="speed">
        <label>속도</label>
        <Slider
          min={1}
          max={10}
          defaultValue={speed}
          handle={handleChangeSpeed}
          style={{
            marginRight: 5,
          }}
        />
      </div>
      <button
        className={classNames('play-button', { playing })}
        onClick={handleClickButton}
      />
    </div>
  );
}

export default Dashboard;
