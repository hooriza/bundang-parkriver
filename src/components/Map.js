import React, { Suspense } from 'react';
import { Canvas } from 'react-three-fiber';

import Controls from './Controls';
import Sunlight from './Sunlight';
import Building from './Building';
import Terrain from './Terrain';

function Map() {
  return (
    <Canvas
      style={{ background: '#DDDDDD' }}
      shadowMap
      background={0xff0000}
      camera={{
        zoom: 1,
        near: 0.1,
        far: 5000,
        fov: 75,
        position: [0, 500, 300],
        look: [0, 0, 0],
      }}
    >
      <ambientLight color={0x222222} />
      <Suspense fallback={null}>
        <Sunlight />
        <Building position={[-57, -10]} scale={0.87} />
        <Terrain position={[-57, -10]} scale={0.87} />
      </Suspense>
      <Controls />
    </Canvas>
  );
}

export default Map;
