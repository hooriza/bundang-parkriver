import React, { useRef } from 'react';
import { extend, useFrame, useThree } from 'react-three-fiber';
import { OrbitControls } from 'three/examples/jsm/controls/OrbitControls';

extend({ OrbitControls });

let reservedLookAt = null;

function Controls() {
  const controls = useRef();
  const { camera, gl } = useThree();
  useFrame(() => {
    if (reservedLookAt) {
      controls.current.target.set(...reservedLookAt);
      reservedLookAt = null;
    } else {
      controls.current.update();
    }
  });
  return (
    <orbitControls
      ref={controls}
      args={[camera, gl.domElement]}
      enableDamping
      enableKeys={false}
      dampingFactor={0.1}
      rotateSpeed={0.5}
      minPolarAngle={Math.PI / -2}
      maxPolarAngle={Math.PI / 2.1}
      maxDistance={1000}
      minDistance={0}
    />
  );
}

export function lookAt(x, y, z) {
  reservedLookAt = [x, y, z];
}

export default Controls;
