import React from 'react';
import * as THREE from 'three';
import { GLTFLoader } from 'three/examples/jsm/loaders/GLTFLoader';
import { useLoader } from 'react-three-fiber';

function Terrain({ position, scale = 1 }) {
  const [gltf] = useLoader(GLTFLoader, ['./terrain.glb']);

  gltf.scene.traverse(node => {
    if (node instanceof THREE.Mesh) {
      node.castShadow = true;
      node.receiveShadow = true;
    }
  });

  return (
    <React.Fragment>
      <primitive
        object={gltf.scene}
        position={[position[0], 0, position[1]]}
        scale={[scale, scale, scale]}
        castShadow
        receiveShadow
      />
      <mesh position={[0, -15, 0]} castShadow rotation={[-Math.PI / 2, 0, 0]}>
        <planeGeometry attach="geometry" args={[5000, 5000]} />
        <meshBasicMaterial attach="material" color={0xdddddd} />
      </mesh>
    </React.Fragment>
  );
}

export default Terrain;
