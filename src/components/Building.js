import React, { useEffect, useCallback, useRef } from 'react';
import * as THREE from 'three';
import { GLTFLoader } from 'three/examples/jsm/loaders/GLTFLoader';
import { useLoader, useThree } from 'react-three-fiber';

import { lookAt } from '../components/Controls';

const raycaster = new THREE.Raycaster();
const mouse = new THREE.Vector2();

let mesh = null;
let orthogonalVector = null;

function Bulding({ position, scale = 1 }) {
  const [gltf] = useLoader(GLTFLoader, ['./park-river.glb']);
  const { camera } = useThree();

  const arrowRef = useRef();

  gltf.scene.traverse(node => {
    if (node instanceof THREE.Mesh) {
      node.castShadow = true;
      node.receiveShadow = true;
      mesh = node;
    }
  });

  const handleMouseMove = useCallback(
    event => {
      event.preventDefault();

      mouse.x = (event.clientX / window.innerWidth) * 2 - 1;
      mouse.y = -(event.clientY / window.innerHeight) * 2 + 1;

      raycaster.setFromCamera(mouse, camera);
      const intersects = raycaster.intersectObject(mesh);

      if (intersects.length > 0) {
        const position = intersects[0].point;
        const cameraGap = new THREE.Vector3(
          camera.position.x - position.x,
          camera.position.y - position.y,
          camera.position.z - position.z,
        );

        const candis = [
          intersects[0].face.normal,
          intersects[0].face.normal.clone().multiplyScalar(-1),
        ];

        // 왜때문인지 모르겠음 그냥 이렇게 하자;;;
        orthogonalVector =
          cameraGap.distanceTo(candis[0]) > cameraGap.distanceTo(candis[1])
            ? candis[1]
            : candis[0];

        if (Math.abs(orthogonalVector.y) < 0.1) {
          arrowRef.current.setDirection(orthogonalVector);
          arrowRef.current.position.copy(position);
          arrowRef.current.visible = true;
          return;
        }
      }

      orthogonalVector = null;
      arrowRef.current.visible = false;
    },
    [camera],
  );

  const handleMouseClick = useCallback(
    event => {
      event.preventDefault();
      if (!orthogonalVector) return;

      const position = arrowRef.current.position;
      const targetPos = [
        position.x - orthogonalVector.x * -10,
        position.y - orthogonalVector.y * -10,
        position.z - orthogonalVector.z * -10,
      ];

      camera.position.copy(position);
      lookAt(...targetPos);

      orthogonalVector = null;
      arrowRef.current.visible = false;
    },
    [camera.position],
  );

  useEffect(() => {
    document.addEventListener('mousemove', handleMouseMove);
    document.addEventListener('dblclick', handleMouseClick);
    return () => {
      document.removeEventListener('mousemove', handleMouseMove);
      document.removeEventListener('dblclick', handleMouseClick);
    };
  });

  return (
    <React.Fragment>
      <arrowHelper
        ref={arrowRef}
        args={[
          new THREE.Vector3(0, 0, 0),
          new THREE.Vector3(0, 0, 0),
          30,
          0xff0000,
          10,
          10,
        ]}
        visible={false}
      />
      <primitive
        object={gltf.scene}
        position={[position[0], 0, position[1]]}
        scale={[scale, scale, scale]}
        castShadow
        receiveShadow
      />
    </React.Fragment>
  );
}

export default Bulding;
